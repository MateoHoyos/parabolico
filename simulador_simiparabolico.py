from math import *
from graphics import *
#https://mcsp.wartburg.edu/zelle/python/graphics.py
#def calcularDatosTiroParabolico(Vo,a):

ventana= GraphWin ("Simulador Parabólico",1000,1000)
ventana.setCoords(0,0,1000,1000)

texto=Entry(Point(50,100),20)

t=0
g=9.8
y=0
Vo=100
a=radians(45)   

while y>=0:
        
        x=Vo*cos(a)*t
        y=Vo*sin(a)*t -(1/2)*g*pow(t,2)
        print (x,y)
        circulo=Circle(Point(x,y) , 2)
        circulo.setFill("Blue")
        circulo.draw(ventana)
        t+=0.01
print(t)
#calcularDatosTiroParabolico(100,radians(45))



ventana.getMouse()
ventana.close()
